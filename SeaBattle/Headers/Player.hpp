#pragma once

#include "Ship.hpp"

class Player {
	int mouseX, mouseY, begX, field[10][10] = { 0 }, dir = 0, fleetSize = 0, score = 0;
	Event event;
    Ship fleet[10], ships[10] = { Ship(-0.5, -2.75, 0, 4, 1), Ship(-0.5, -1.5, 0, 3, 1),
                                Ship(2.75, -1.5, 0, 3, 1), Ship(3.75, -2.75, 0, 2, 1),
                                Ship(6, -2.75, 0, 2, 1), Ship(6, -1.5, 0, 2, 1),
                                Ship(8.25, -2.75, 0, 1, 1), Ship(8.25, -1.5, 0, 1, 1),
                                Ship(9.5, -2.75, 0, 1, 1), Ship(9.5, -1.5, 0, 1, 1)};
    Ship *ship = NULL;
    Texture scopeTexture;
	bool filled = false, pressed = false;
public:
	Player();
	void update(Vector2i, Event);
	void fill();
	bool isFilled();
	Vector2i shot();
	Ship *getFleet();
	int hit(Vector2i);
	void feedback(Ship*, int);
	bool isWin();
	void drawField(RenderWindow&);
	void draw(RenderWindow&);
};
