#pragma once

#include "SFML/Graphics.hpp"

using namespace sf;

class Tile {
	double x = 0, y = 0;
	bool life = true;
public:
	Tile();
	Tile(double, double);
	Vector2f getCoord();
	bool isLife();
	void kill();
	void draw(RenderWindow&, Color, int = 1);
};
