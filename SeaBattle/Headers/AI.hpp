#pragma once

#include "Ship.hpp"
#include "Stack.hpp"

class Comp {
	int field[10][10] = { 0 }, ships[10] = { 0 }, fleetSize = 0, phase = 1, currentShip = 0, finishX = 10, finishY = 10, dir = 10, shots = 0;
	Ship fleet[10], *ship;
	Stack shotStack, checkStack;
	bool finishOf = false;
public:
	Comp();
	void fill();
	Vector2i shot();
	int hit(Vector2i);
	void capture(Vector2i);
	void feedback(Ship*, int, Vector2i);
	bool isWin();
	Ship *getFleet();
	int getShots();
	void drawField(RenderWindow&);
	void draw(RenderWindow&, bool);
};
