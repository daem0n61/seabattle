#pragma once

#include "SFML/Graphics.hpp"

using namespace sf;

class GameInfo {
	Font font;
	Text text;
public:
	GameInfo();
	void update(String, int);
	void draw(RenderWindow&);
};
