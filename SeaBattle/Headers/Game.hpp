#pragma once

#include "Player.hpp"
#include "AI.hpp"
#include "GameInfo.hpp"

class Game {
	RenderWindow window;
	Event event;
	Vector2i coord, infUpd;
	GameInfo info;
	Player player;
	Comp comp;
	bool plTurn = false, compTurn = true;
	void update();
	void draw();
public:
	Game();
	void start();
};
