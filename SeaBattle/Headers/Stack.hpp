#pragma once

#include "SFML/system/Vector2.hpp"

using namespace sf;

struct StackElem {
	Vector2i coord;
	StackElem *next;
};

class Stack {
	StackElem *top = 0;
	int size = 0;
public:
	void del(Vector2i);
	bool check(Vector2i);
	void push(Vector2i);
	Vector2i pop(int = 0);
	void operator==(Stack stack);
	bool isEmpty();
	int getSize();
};
