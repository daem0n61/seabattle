#pragma once

#include "SFML/Graphics.hpp"
#include <vector>
#include "Tile.hpp"

using namespace sf;
using namespace std;

class Ship {
	vector<Tile> tiles;
	int updTime = 50, frame;
public:
	Ship();
	Ship(double, double, int, int, int = 0);
	void create(double, double, int, int, int = 0);
	bool aroundCheck(Ship[10], int = 0, int = 0);
	bool check(Vector2i, int = 0);
	bool isLife();
	int getSize();
	vector<Tile> getTiles();
	void update();
	void draw(RenderWindow&, bool, int = 0);
};
