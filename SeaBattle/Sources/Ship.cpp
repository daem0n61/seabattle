#include "Ship.hpp"

Ship::Ship() {
}

Ship::Ship(double x, double y, int dir, int _size, int _frame) {
	frame = _frame;
	for (int i = 0; i < _size; i++) {
		if (dir == 0) tiles.push_back(Tile(x + i, y));
		else tiles.push_back(Tile(x, y + i));
	}
}

void Ship::create(double x, double y, int dir, int _size, int _frame) {
	frame = _frame;
	for (int i = 0; i < _size; i++) {
		if (dir == 0) tiles.push_back(Tile(x + i, y));
		else tiles.push_back(Tile(x, y + i));
	}
}

bool Ship::aroundCheck(Ship fleet[10], int fleetSize, int offset) {
	for (int thisTile = 0; thisTile < tiles.size(); thisTile++) {
		int x = tiles[thisTile].getCoord().x, y = tiles[thisTile].getCoord().y;
		if (x > 9 + offset || y > 9) return false;
		for (int i = x - 1; i < x + 2; i++)
			for (int j = y - 1; j < y + 2; j++) {
				if (i < offset || i > 9 + offset || j < 0 || j > 9) continue;
				for (int num = 0; num < fleetSize; num++)
					for (int tile = 0; tile < fleet[num].getSize(); tile++)
						if (fleet[num].tiles[tile].getCoord() == Vector2f(i, j)) return false;
			}
	}
	return true;
}

bool Ship::check(Vector2i coord, int offset) {
	for (int i = 0; i < getSize(); i++)
		if (tiles[i].getCoord() == Vector2f(coord.x + offset, coord.y)) { tiles[i].kill(); return true; };
	return false;
}

bool Ship::isLife() {
	for (int i = 0; i < getSize(); i++) if (tiles[i].isLife()) return true;
	return false;
}

int Ship::getSize() {
	return tiles.size();
}

std::vector<Tile> Ship::getTiles() {
	return tiles;
}

void Ship::update() {
	if (frame > 0 && frame < updTime) frame++;
	else if (frame != 0) frame = 1;
}

void Ship::draw(RenderWindow &window, bool visible, int type) {
	if (frame > updTime / 2) return;
	for (int i = 0; i < getSize(); i++) {
		if (isLife()) tiles[i].draw(window, Color::Yellow, type);
		else tiles[i].draw(window, Color::Red);
	}
}
