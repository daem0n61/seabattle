#include "Stack.hpp"

void Stack::del(Vector2i coord) {
	if (size == 0) return;
	StackElem *iter = top, *temp;
	if (top->coord == coord) {
		temp = top;
		top = temp->next;
		size--;
		delete temp; return;
	}
	while (iter->next != 0) {
		if (iter->next->coord == coord) {
			temp = iter->next;
			iter->next = temp->next;
			size--;
			delete temp; return;
		}
		iter = iter->next;
	}
}

bool Stack::check(Vector2i coord) {
	StackElem *iter = top, *temp;
	while (iter != 0) {
		if (iter->coord == coord) return true;
		iter = iter->next;
	}
	return false;
}

void Stack::push(Vector2i coord) {
	if (check(coord)) del(coord);
	StackElem *temp = new StackElem;
	temp->coord = coord;
	temp->next = top;
	top = temp;
	size++;
}

Vector2i Stack::pop(int num) {
	StackElem *temp = top;
	Vector2i coord;
	if (num == 0) {
		top = temp->next;
		coord = temp->coord;
	}
	else {
		StackElem *iter = top; int i = 0;
		while (i < num - 1) {
			iter = iter->next;
			i++;
		}
		temp = iter->next;
		iter->next = temp->next;
		coord = temp->coord;
	}
	delete temp;
	size--;
	return coord;
}

void Stack::operator==(Stack stack) {
	StackElem *iter = top, *temp;
	while (iter != 0) {
		if (!stack.check(iter->coord)) {
			del(iter->coord);
			*this == stack;
			return;
		}
		else iter = iter->next;
	}
}

bool Stack::isEmpty() {
	if (top == 0) return true;
	else return false;
}

int Stack::getSize() {
	return size;
}
