#include <sstream>
#include <SFML/Window.hpp>
#include "ResourcePath.hpp"
#include "Game.hpp"

using namespace std;

Game::Game() {
	window.create(VideoMode(1280, 720), "Sea Battle", Style::Close);
    Image icon;
    if (icon.loadFromFile(resourcePath() + "Icon.png"))
        window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
}

void Game::update() {
	player.update(Mouse::getPosition(window), event);

	if (!player.isFilled()) player.fill();
	else {
		if (plTurn) {
			coord = player.shot();
			if (coord != Vector2i(10, 10)) {
				int num = comp.hit(coord);
				if (num < 10) player.feedback(comp.getFleet(), num);
				else {
					compTurn = true; plTurn = false;
				}
			}
		}
		if (compTurn) {
			coord = comp.shot();
			infUpd = coord;
			int num = player.hit(coord);
			if (num < 10) comp.feedback(player.getFleet(), num, coord);
			else {
				compTurn = false; plTurn = true;
			}
		}
	}
    
	stringstream str;
	if (player.isWin()) {
		compTurn = plTurn = false;
		info.update("!!!YOU WIN!!!", 400);
	}
	else if (comp.isWin()) {
		compTurn = plTurn = false;
		str << comp.getShots();
		info.update("I WIN WITH " + str.str() + " SHOTS", 250);
	}
	else {
		str << infUpd.x << ' ' << infUpd.y;
		info.update("I SHOT IN " + str.str(), 350);
	}
}

void Game::draw() {
	window.clear();
	player.drawField(window);
	comp.drawField(window);
	comp.draw(window, 0);
	player.draw(window);
	if (player.isFilled()) info.draw(window);
	window.display();
}

void Game::start() {
	window.setVerticalSyncEnabled(true);
	window.setActive();
	comp.fill();

	while (window.isOpen()) {
		while (window.pollEvent(event)) {
			if (event.type == Event::Closed) window.close();
		}
		update();
		draw();
	}
}
