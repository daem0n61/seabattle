#include "Tile.hpp"

#include <iostream>

Tile::Tile() {
}

Tile::Tile(double _x, double _y) {
	x = _x; y = _y;
}

Vector2f Tile::getCoord() {
	return Vector2f(x, y);
}

bool Tile::isLife() {
	return life;
}

void Tile::kill() {
	life = false;
}

void Tile::draw(RenderWindow &window, Color color, int type) {
    RectangleShape tile;
	tile.setSize(Vector2f(50, 50));
	tile.setPosition(100 + x * 51, 155 + y * 51);
	if (life) {
		if (type == 0) tile.setFillColor(Color(165, 42, 42));
		else tile.setFillColor(Color::Cyan);
	}
	else tile.setFillColor(color);
	window.draw(tile);
}
