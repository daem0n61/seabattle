#include "AI.hpp"

Comp::Comp() {
}

void Comp::fill() {
	int x, y, dir, fleetSize = 0, arr1[100], arr2[100];
	int offset = 11;
	for (int i = 0; i < 100; i++) { arr1[i] = i % 10; arr2[i] = i / 10; }
	for (int i = 99; i >= 0; i--) checkStack.push(Vector2i(arr1[i], arr2[i]));
	for (int y = 0; y < 10; y++)
		for (int x = 0; x < 10; x++)
			if (x + y == 3 || x + y == 7 || x + y == 11 || x + y == 15) shotStack.push(Vector2i(x, y));
	time_t t;
	time(&t);
	srand((unsigned int)t);
	{
		x = rand() % 6 + offset; y = rand() % 6; dir = rand() % 2;
		fleet[fleetSize].create(x, y, dir, 4);
		fleetSize++;
	}
	while (fleetSize < 3) {
		x = rand() % 7 + offset; y = rand() % 7; dir = rand() % 2;
		ship = new Ship(x, y, dir, 3);
		if (!ship->aroundCheck(fleet, fleetSize, offset)) continue;
		fleet[fleetSize].create(x, y, dir, 3);
		fleetSize++;
	}
	while (fleetSize < 6) {
		x = rand() % 8 + offset; y = rand() % 8; dir = rand() % 2;
		ship = new Ship(x, y, dir, 2);
		if (!ship->aroundCheck(fleet, fleetSize, offset)) continue;
		fleet[fleetSize].create(x, y, dir, 2);
		fleetSize++;
	}
	while (fleetSize < 10) {
		x = rand() % 9 + offset; y = rand() % 9; ship = new Ship(x, y, dir, 1);
		if (!ship->aroundCheck(fleet, fleetSize, offset)) continue;
		fleet[fleetSize].create(x, y, dir, 1);
		fleetSize++;
	}
}

Vector2i Comp::shot() {
	if (phase == 2) {
		for (int x = 0; x < 10; x++)
			for (int y = 0; y < 10; y++)
				if (x + y == 1 || x + y == 5 || x + y == 9 || x + y == 13 || x + y == 17) shotStack.push(Vector2i(x, y));
		phase++;
	}
	if (phase == 4) {
		int arr1[100], arr2[100];
		for (int i = 0; i < 100; i++) { arr1[i] = i % 10; arr2[i] = i / 10; }
		for (int i = 99; i >= 0; i--) shotStack.push(Vector2i(arr1[i], arr2[i]));
		phase++;
	}
	shotStack == checkStack;
	time_t t;
	time(&t);
	srand((unsigned int)t);
	while (true) {
		Vector2i coord;
		if (finishOf) coord = shotStack.pop();
		else coord = shotStack.pop(rand() % shotStack.getSize());
		if (field[coord.x][coord.y] != 0) continue;
		field[coord.x][coord.y] = 1;
		checkStack.del(Vector2i(coord.x, coord.y));
		shots++;
		return coord;
	}
}

int Comp::hit(Vector2i coord) {
	int num = 10;
	for (int i = 0; i < 10; i++) if (fleet[i].check(coord, 11)) num = i;
	return num;
}

void Comp::capture(Vector2i coord) {
	checkStack.del(Vector2i(coord.x - 1, coord.y - 1));
	checkStack.del(Vector2i(coord.x, coord.y - 1));
	checkStack.del(Vector2i(coord.x + 1, coord.y - 1));
	checkStack.del(Vector2i(coord.x - 1, coord.y));
	checkStack.del(Vector2i(coord.x + 1, coord.y));
	checkStack.del(Vector2i(coord.x - 1, coord.y + 1));
	checkStack.del(Vector2i(coord.x, coord.y + 1));
	checkStack.del(Vector2i(coord.x + 1, coord.y + 1));
}

void Comp::feedback(Ship *fleet, int num, Vector2i coord) {
	int pushedTiles = 4;
	if (finishX == coord.x) dir = 0;
	else finishX = coord.x;
	if (finishY == coord.y) dir = 1;
	else finishY = coord.y;
	if (dir != 10) pushedTiles = 2;
	field[coord.x][coord.y] = 1;
	if (fleet[num].isLife()) {
		finishOf = true;
		ships[currentShip]++;
		checkStack.del(Vector2i(coord.x - 1, coord.y - 1));
		checkStack.del(Vector2i(coord.x + 1, coord.y - 1));
		checkStack.del(Vector2i(coord.x - 1, coord.y + 1));
		checkStack.del(Vector2i(coord.x + 1, coord.y + 1));
		if ((coord.x == 0 || coord.x == 9) && pushedTiles > 2) pushedTiles--;
		if ((coord.y == 0 || coord.y == 9) && pushedTiles > 2) pushedTiles--;
		int size = pushedTiles, *temp = new int[size];
		for (int i = 0; i < size; i++) temp[i] = 10;
		while (pushedTiles != 0) {
			int rnd = rand() % 4;
			bool check = true;
			for (int i = 0; i < size; i++) if (rnd == temp[i]) check = false;
			if (!check) continue;
			switch (rnd) {
			case 0: if (coord.y != 0 && (dir == 0 || dir == 10)) { shotStack.push(Vector2i(coord.x, coord.y - 1)); temp[size - pushedTiles] = rnd; pushedTiles--; } break;
			case 1:	if (coord.x != 0 && (dir == 1 || dir == 10)) { shotStack.push(Vector2i(coord.x - 1, coord.y)); temp[size - pushedTiles] = rnd; pushedTiles--; } break;
			case 2: if (coord.x != 9 && (dir == 1 || dir == 10)) { shotStack.push(Vector2i(coord.x + 1, coord.y)); temp[size - pushedTiles] = rnd; pushedTiles--; } break;
			case 3: if (coord.y != 9 && (dir == 0 || dir == 10)) { shotStack.push(Vector2i(coord.x, coord.y + 1)); temp[size - pushedTiles] = rnd; pushedTiles--; } break;
			}
		}
		delete[] temp;
		return;
	}
	finishX = finishY = dir = 10;
	for (int tile = 0; tile < fleet[num].getSize(); tile++) capture(Vector2i(fleet[num].getTiles()[tile].getCoord().x, fleet[num].getTiles()[tile].getCoord().y));
	ships[currentShip]++;
	currentShip++;
	int counter = 0;
	if (phase == 1) {
		for (int num = 0; num < 10; num++) if (ships[num] == 4) counter++;
		if (counter == 1) phase++;
	}
	if (phase == 3) {
		for (int num = 0; num < 10; num++) if (ships[num] == 2) counter++;
		if (counter == 3) phase++;
	}
	if (phase == 5) {
		for (int num = 0; num < 10; num++) if (ships[num] == 1) counter++;
		if (counter == 4) phase++;
	}
	finishOf = false;
}

bool Comp::isWin() {
	if (phase == 6) return true;
	else return false;
}

Ship *Comp::getFleet() {
	return fleet;
}

int Comp::getShots() {
	return shots;
}

void Comp::drawField(RenderWindow &window) {
	RectangleShape tile;
	tile.setSize(Vector2f(50, 50));
	for (int j = 0; j < 10; j++)
		for (int i = 0; i < 10; i++) {
			tile.setPosition(100 + i * 51, 155 + j * 51);
			if (field[i][j] == 0) tile.setFillColor(Color::Cyan);
			if (field[i][j] == 1) tile.setFillColor(Color::Blue);
			if (field[i][j] == 2) tile.setFillColor(Color::Black);
			window.draw(tile);
		}
}

void Comp::draw(RenderWindow &window, bool visible) {
	for (int i = 0; i < 10; i++) fleet[i].draw(window, visible, 1);
}
