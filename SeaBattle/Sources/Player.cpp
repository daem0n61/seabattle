#include "ResourcePath.hpp"
#include "Player.hpp"

Player::Player() {
}

void Player::update(Vector2i mouse, Event _event) {
    scopeTexture.loadFromFile(resourcePath() + "Scope.png");
	if (filled) begX = 661;
	else begX = 100;

	if (mouse.x < begX) mouseX = -1;
	else mouseX = (mouse.x - begX) / 51;
	if (mouse.y < 155) mouseY = -1;
	else mouseY = (mouse.y - 155) / 51;

	event = _event;

	if (!filled) ships[fleetSize].update();
}

void Player::fill() {
	if (mouseX < 0 || mouseX > 9 || mouseY < 0 || mouseY > 9) return;
    delete ship;
	if (event.type == Event::MouseButtonPressed && event.mouseButton.button == Mouse::Right) pressed = true;
	if (event.type == Event::MouseButtonReleased && event.mouseButton.button == Mouse::Right && pressed) {
		switch (dir)
		{
		case 0: dir = 1; break;
		case 1: dir = 0; break;
		}
		pressed = false;
	}
	if (fleetSize < 1) {
		ship = new Ship(mouseX, mouseY, dir, 4);
		if (ship->aroundCheck(fleet)) {
			if (event.type == Event::MouseButtonPressed && event.mouseButton.button == Mouse::Left) pressed = true;
			if (event.type == Event::MouseButtonReleased && event.mouseButton.button == Mouse::Left && pressed) {
				pressed = false;
				fleet[fleetSize].create(mouseX, mouseY, dir, 4);
				fleetSize++;
			}
		}
	}
	if (fleetSize > 0 && fleetSize < 3) {
		ship = new Ship(mouseX, mouseY, dir, 3);
		if (ship->aroundCheck(fleet, fleetSize)) {
			if (event.type == Event::MouseButtonPressed && event.mouseButton.button == Mouse::Left) pressed = true;
			if (event.type == Event::MouseButtonReleased && event.mouseButton.button == Mouse::Left && pressed) {
				pressed = false;
				fleet[fleetSize].create(mouseX, mouseY, dir, 3);
				fleetSize++;
			}
		}
	}
	if (fleetSize > 2 && fleetSize < 6) {
		ship = new Ship(mouseX, mouseY, dir, 2);
		if (ship->aroundCheck(fleet, fleetSize)) {
			if (event.type == Event::MouseButtonPressed && event.mouseButton.button == Mouse::Left) pressed = true;
			if (event.type == Event::MouseButtonReleased && event.mouseButton.button == Mouse::Left && pressed) {
				pressed = false;
				fleet[fleetSize].create(mouseX, mouseY, dir, 2);
				fleetSize++;
			}
		}
	}
	if (fleetSize > 5 && fleetSize < 10) {
		ship = new Ship(mouseX, mouseY, dir, 1);
		if (ship->aroundCheck(fleet, fleetSize)) {
			if (event.type == Event::MouseButtonPressed && event.mouseButton.button == Mouse::Left) pressed = true;
			if (event.type == Event::MouseButtonReleased && event.mouseButton.button == Mouse::Left && pressed) {
				pressed = false;
				fleet[fleetSize].create(mouseX, mouseY, dir, 1);
				fleetSize++;
			}
		}
	}
	if (fleetSize == 10) filled = true;
}

bool Player::isFilled() {
	return filled;
}

Vector2i Player::shot() {	
	if (mouseX < 0 || mouseX > 9 || mouseY < 0 || mouseY > 9) return Vector2i(10, 10);
	if (event.type == Event::MouseButtonPressed && event.mouseButton.button == Mouse::Left) pressed = true;
	if (event.type == Event::MouseButtonReleased && event.mouseButton.button == Mouse::Left && pressed) {
		pressed = false;
		if (field[mouseY][mouseX] == 1) return Vector2i(10, 10);
		field[mouseY][mouseX] = 1;
		return Vector2i(mouseX, mouseY);
	}
	return Vector2i(10, 10);
}

Ship * Player::getFleet() {
	return fleet;
}

int Player::hit(Vector2i coord) {
	int num = 10;
	for (int i = 0; i < 10; i++) if (fleet[i].check(coord)) num = i;
	return num;
}

void Player::feedback(Ship *fleet, int num) {
	if (!fleet[num].isLife()) score++;
}

bool Player::isWin() {
	if (score == 10) return true;
	return false;
}

void Player::drawField(RenderWindow &window) {
	RectangleShape tile;
	tile.setSize(Vector2f(50, 50));
	for (int j = 0; j < 10; j++)
		for (int i = 0; i < 10; i++) {
			tile.setPosition(661 + i * 51, 155 + j * 51);
			if (field[j][i] == 0) tile.setFillColor(Color::Cyan);
			else tile.setFillColor(Color::Blue);
			window.draw(tile);
		}
}

void Player::draw(RenderWindow &window) {
	for (int i = 0; i < 10; i++) fleet[i].draw(window, 0);
	for (int i = fleetSize; i < 10; i++) ships[i].draw(window, true);
	if (mouseX < 0 || mouseX > 9 || mouseY < 0 || mouseY > 9) return;
    
	RectangleShape scope;
	scope.setSize(Vector2f(50, 50));
    if (filled) {
        //scope.setFillColor(Color::Transparent);
        scope.setTexture(&scopeTexture);
    }
	else scope.setFillColor(Color(100, 15, 15));

	scope.setPosition(begX + mouseX * 51, 155 + mouseY * 51);
	window.draw(scope);

	if (!filled && ship->aroundCheck(fleet, fleetSize)) ship->draw(window, 0);
}
