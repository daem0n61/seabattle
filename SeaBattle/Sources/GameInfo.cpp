#include "GameInfo.hpp"
#include "ResourcePath.hpp"

GameInfo::GameInfo() {
    if (!font.loadFromFile(resourcePath() + "Font.ttf")) {
        printf("Font not loaded");
    }
	text.setFont(font);
	text.setCharacterSize(100);
}

void GameInfo::update(String str, int x) {
	text.setPosition(x, 20);
	text.setString(str);
}

void GameInfo::draw(RenderWindow &window) {
	window.draw(text);
}
